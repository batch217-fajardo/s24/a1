// console.log("Hello World");

const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);

const fullAddress = ["258", "Washington Ave NW", "California",90011];


const [houseNumber, street, state, zipCode] = fullAddress;
console.log(`I live at ${houseNumber}, ${street}, ${state} ${zipCode}`);


const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	lengthFt: 20,
	lengthIn: 3,
};


// oobject destructuring
const {name, species, weight, lengthFt, lengthIn} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${lengthFt} ft ${lengthIn}.`);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(`${number}`);
});


let reducedNumber = numbers.reduce((a, b) => {
	return a + b;
	
});
console.log(reducedNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed

	}
}
const myDog = new Dog();
console.log(myDog);

myDog.name = "Frankie";
myDog.age = "5";
myDog.breed = "Miniature Dachshund";

console.log(myDog);




